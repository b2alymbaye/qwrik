export const Groups = new Mongo.Collection('groups');

export const GroupSchema = new SimpleSchema({
  _id: {
    type: String,
  },
  userId: {
    type: String,
  },
  name: {
    type: String,
  },
  slug: {
    type: String,
  },
  users: {
    type: [String],
  },
  isPrivate: {
    type: Boolean,
  },
  pictureId: {
    type: String,
    optional: true,
  },
  createdAt: {
    type: Date,
  },
});

export const isInGroup = function (userId, group) {
  checkRequiredField(group, 'users');
  return group.users.includes(userId);
};

function checkRequiredField(group, field) {
  if (!group || !group[field]) {
    throw new Error('Required field not found!');
  }
}