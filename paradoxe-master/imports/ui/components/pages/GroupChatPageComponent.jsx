import React from 'react';

import { ChatContainer } from '/imports/ui/containers/parts/chat/ChatContainer';
import { NotFoundPageComponent } from '/imports/ui/components/pages/NotFoundPageComponent';
import { SpinnerComponent } from '/imports/ui/components/parts/app/spinner/SpinnerComponent';

export const GroupChatPageComponent = React.createClass({
  propTypes: {
    user: React.PropTypes.object.isRequired,
    group: React.PropTypes.object,
    ready: React.PropTypes.bool.isRequired,
    isInGroup: React.PropTypes.bool.isRequired,
    loadMessages: React.PropTypes.func.isRequired,
  },
  getInitialState: function () {
    return {
      ready: false,
    };
  },
  componentWillMount: function () {
    this.props.loadMessages();
  },
  componentWillReceiveProps: function (nextProps) {
    if (!this.state.ready && nextProps.ready) {
      this.setState({
        ready: true,
      });
    }
    
    if (this.props.group && !nextProps.group
      || this.props.group && nextProps.group && this.props.group._id !== nextProps.group._id) {
      this.props.loadMessages(nextProps.params.groupId);
      this.setState({
        ready: false,
      });
    }
  },
  render: function () {
    if (!this.state.ready) {
      return (
        <SpinnerComponent />
      );
    }

    return this.props.isInGroup
      ? <div>
      <p>hey</p>
      {/*<ChatContainer contact={this.props.contact}/>*/}
      </div>
      : <NotFoundPageComponent />;
  },
});
