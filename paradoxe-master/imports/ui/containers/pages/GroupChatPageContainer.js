import { connect } from 'react-redux';

import { defaultGroupState } from '/imports/reducers/groups';
import { loadGroupChat } from '/imports/actions/groups/load';

import { GroupChatPageComponent } from '/imports/ui/components/pages/GroupChatPageComponent';

const mapStateToProps = (state, props) => {
  const chatState = state.groups[props.params.groupId] || defaultGroupState;
  return {
    user: state.user,
    group: chatState.group,
    ready: chatState.ready,
    isInGroup: chatState.isInGroup
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    loadMessages: (groupId = props.params.groupId) => {
      dispatch(loadGroupChat(groupId));
    },
  };
};

export const GroupChatPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(GroupChatPageComponent);
