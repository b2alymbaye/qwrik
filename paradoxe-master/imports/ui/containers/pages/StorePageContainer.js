import { connect } from 'react-redux';

import { defaultChatState } from '/imports/reducers/chats';
import { loadChat } from '/imports/actions/chats/load';

import { StorePageComponent } from '/imports/ui/components/pages/StorePageComponent';

const mapStateToProps = (state, props) => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    
  };
};

export const StorePageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(StorePageComponent);
