import { connect } from 'react-redux';

import { increaseChatLimit } from '/imports/actions/chats/message/limit';

import { GroupChatComponent } from '/imports/ui/components/parts/group/GroupChatComponent';

const mapStateToProps = (state, props) => {
  const prop = props.contact
    ? props.contact.username
    : props.group._id;
  
  
  
  return {
    user: state.user,
    ready: state.chats[props.contact.username].ready,
    messages: state.chats[props.contact.username].messages,
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    increaseChatLimit: () => {
      dispatch(increaseChatLimit(props.contact.username));
    },
  };
};

export const GroupChatContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatComponent);
