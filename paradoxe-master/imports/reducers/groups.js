import { MESSAGES_PER_PAGE } from '/imports/api/collections/messages';

import { GROUP_SUBSCRIPTION_READY, GROUP_SUBSCRIPTION_CHANGED } from '/imports/actions/groups/load';
import { GROUP_INCREASE_LIMIT } from '/imports/actions/groups/message/limit';
//import { CHAT_VIDEO_UPDATE } from '/imports/actions/chats/video/update';
//import { CHAT_VIDEO_RESET } from '/imports/actions/chats/video/reset';
//import { CHAT_FILES_DROPPED } from '/imports/actions/chats/file/dropped';
//import { CHAT_FILES_UPLOAD_STARTED } from '/imports/actions/chats/file/upload';
//import { CHAT_FILE_CLEAN } from '/imports/actions/chats/file/clean';

const initialState = {};

//const defaultVideoCallState = {
//  peer: null,
//  stream: null,
//  call: null,
//  contactStream: null,
//  userPeerId: null,
//  contactPeerId: null,
//  callMessageId: null,
//  isRinging: false,
//  isHangingUp: false,
//};

export const defaultGroupState = {
  ready: false,
  messages: [],
  messagesLimit: MESSAGES_PER_PAGE,
  users: [],
  group: null,
  isInGroup: false,
//  files: [],
//  localFiles: [],
//  uploadHandlers: [],
//  videoCall: {
//    ...defaultVideoCallState,
//  },
};

export function groups(state = initialState, action) {
  let nextState = null;

  switch (action.type) {
    case GROUP_SUBSCRIPTION_READY:
      nextState = getNextState(state, action.data.groupId);
      nextState[action.data.groupId].ready = action.ready;

      return nextState;
    case GROUP_SUBSCRIPTION_CHANGED:
      console.log('action', action);
      if (!action.data.group) {
        return state;
      }

      nextState = getNextState(state, action.data.group._id);
      nextState[action.data.group._id] = {
        ...nextState[action.data.group._id],
        ...action.data,
      };

//      if (!nextState[action.data.groupId].videoCall) {
//        nextState[action.data.groupId].videoCall = {
//          ...defaultVideoCallState,
//        };
//      }

      return nextState;
    case GROUP_INCREASE_LIMIT:
      nextState = getNextState(state, action.groupId);
      nextState[action.groupId].messagesLimit += MESSAGES_PER_PAGE;

//      return nextState;
//    case CHAT_VIDEO_UPDATE:
//      nextState = getNextState(state, action.contact.username);
//
//      nextState[action.contact.username].videoCall = {
//        ...nextState[action.contact.username].videoCall,
//        ...action.data,
//      };
//
//      return nextState;
//    case CHAT_VIDEO_RESET:
//      nextState = getNextState(state, action.contact.username);
//      nextState[action.contact.username].videoCall = {
//        ...defaultVideoCallState,
//      };

//      return nextState;
//    case CHAT_FILES_DROPPED:
//      nextState = getNextState(state, action.contact.username);
//      nextState[action.contact.username].localFiles = [
//        ...nextState[action.contact.username].localFiles,
//        ...action.files,
//      ];
//
//      return nextState;
//    case CHAT_FILES_UPLOAD_STARTED:
//      nextState = getNextState(state, action.contact.username);
//      nextState[action.contact.username].uploadHandlers.push(action.uploadHandler);
//
//      return nextState;
//    case CHAT_FILE_CLEAN:
//      nextState = getNextState(state, action.contact.username);

//      const index = nextState[action.contact.username].localFiles.findIndex(localFile => localFile.id === action.localFileId);
//
//      if (index > -1) {
//        nextState[action.contact.username].localFiles.splice(index, 1);
//      }

      return nextState;
    default:
      return state;
  }
}

function getNextState(state, groupId) {
  const nextState = {
    ...state,
  };

  if (!nextState[groupId]) {
    nextState[groupId] = {
      ...defaultGroupState,
    };
  }

  return nextState;
}
