import { loadGroupChat } from '../load';

export const GROUP_INCREASE_LIMIT = 'GROUP_INCREASE_LIMIT';

export function increaseChatLimit(groupId) {
  return dispatch => {
    dispatch({
      type: GROUP_INCREASE_LIMIT,
      groupId,
    });

    dispatch(loadGroupChat(groupId));
  };
}
