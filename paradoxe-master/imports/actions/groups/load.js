import { Meteor } from 'meteor/meteor';

import { defaultGroupState } from '/imports/reducers/groups';
import { userHasContact } from '/imports/api/collections/users';
import { Messages } from '/imports/api/collections/messages';
import { Files } from '/imports/api/collections/files';
import { Groups, isInGroup } from '/imports/api/collections/groups';

export const GROUP_SUBSCRIPTION = 'GROUP_SUBSCRIPTION';
export const GROUP_SUBSCRIPTION_READY = 'GROUP_SUBSCRIPTION_READY';
export const GROUP_SUBSCRIPTION_CHANGED = 'GROUP_SUBSCRIPTION_CHANGED';

export function loadGroupChat(groupId) {
  return (dispatch, getState) => {
    dispatch({
      type: GROUP_SUBSCRIPTION,
      meteor: {
        subscribe: () => {
          const group = getState().groups[groupId];
          const limit = group ? group.messagesLimit : null;

          return Meteor.subscribe('groups.messages', groupId, limit);
        },
        get: () => {
          const user = Meteor.user();
          const chatState = {
            ...defaultGroupState
          };

          delete chatState.messagesLimit;
//          delete chatState.videoCall;
          delete chatState.localFiles;
          delete chatState.uploadHandlers;

          if (user) {
            chatState.group = Groups.findOne({
              _id: groupId,
            });
            
            if (chatState.group) {
              chatState.isInGroup = isInGroup(user._id, chatState.group);
              if (chatState.isInGroup) {
                chatState.messages = Messages.find({
                  groupId: groupId
                }, {
                  sort: {
                    createdAt: 1
                  }
                }).fetch();

                chatState.users = Meteor.users.find({
                  _id: {
                    $in: _.uniq(_.pluck(chatState.messages, 'userId')),
                  },
                }).fetch();

//                chatState.files = Files.find({
//                  $or: [
//                    { userId: user._id, 'meta.contactId': { $in: [chatState.contact._id] }},
//                    { userId: chatState.contact._id, 'meta.contactId': { $in: [user._id] }},
//                  ]
//                }).fetch();
              }
            }
          }

          return chatState;
        },
        onReadyData: () => ({
          groupId,
        }),
      },
    });
  };
}
