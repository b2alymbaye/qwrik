import { Meteor } from 'meteor/meteor';

import { Messages, MESSAGES_PER_PAGE } from '/imports/api/collections/messages';
import { Files } from '/imports/api/collections/files';
import { Groups, isInGroup } from '/imports/api/collections/groups';

Meteor.publish('groups.messages', function (groupId, limit) {
  if (!limit || limit <= 0) {
    limit = MESSAGES_PER_PAGE;
  }
  
  this.autorun(function () {
    if (!this.userId) {
      return [];
    }
    
    const group = Groups.findOne({
      _id: groupId
    });
    
    if (group && isInGroup(this.userId, group)) {
      return [
        Meteor.users.find({
          _id: {
            $in: [
              ...group.users,
              Meteor.settings.public.bot.id
            ]
          }
        }, {
          fields: {
            username: 1,
            'profile.emailHash': 1,
            'profile.pictureId': 1,
          }
        }),
        Messages.find({
          groupId: group._id
        }, {
          sort: {
            createdAt: -1,
          },
          limit,
        }),
//        Files.find({
//          $or: [
//            { userId: user._id, 'meta.contactId': { $in: [contact._id] } },
//            { userId: contact._id, 'meta.contactId': { $in: [user._id] } },
//            { _id: user.profile.pictureId },
//            { _id: contact.profile.pictureId },
//          ]
//        }).cursor,
      ];
    }
    
    return [];
  });
});
